# coding: utf-8
import json
import sys

from grab import Grab


class Lingualeo(object):
	"""
	Для скачивания словаря пользователя, сохраняет в файле data.json
	Использовать как-то так:
	login = {'email':'login_lingua','password':'pass'}
	Lingualeo(login).get_user_vocab()
	"""

	def __init__(self, login):
		self.login = login
		self.g = Grab()
		self.authorize()
		self.count_page = 1

	def authorize(self):
		"Функция Авторизации"
		login_url = 'http://lingualeo.com/login'
		self.g.setup(post = self.login)
		self.g.go(login_url)
		if "Неправильный email или имя пользователя" in self.g.response.body:
			print "Неправильный email или имя пользователя"
			sys.exit(2)
			

	def get_user_vocab(self, how_vocab = 'no_translate', hash_serv = "2014"):
		"Основная функция получения словаря, рекурсия если несколько страниц."
		url_new = 'http://lingualeo.com/userdict/json'
		variants_dict = ['all', 'learned', 'learning', 'no_translate']
		if how_vocab not in variants_dict:
			print "Неправильно указан тип желаемого:%s" % variants_dict 
			sys.exit(2)
		post = {
				'sortBy':'date',
				'wordType':'0',
				'filter':how_vocab,
				'page':self.count_page,
				'_hash':hash_serv,
				}
		self.g.setup(post = post)
		self.g.go(url_new)
		resp = self.g.response.body
		data_json = json.loads(resp)
		new_hash = data_json["_hash"]
		if "Server hash has been updated" in resp:
			self.get_user_vocab(how_vocab, new_hash)
		else:
			open('data.json', 'a').write(resp+'\n')
			more = data_json.get("show_more", None)
			if more:
				self.count_page += 1
				print "Очень много слов, работаю" % self.count_page
				self.get_user_vocab(how_vocab, new_hash)

if __name__ == '__main__':
	login = {'email':'login_lingua','password':'pass'}
	Lingualeo(login).get_user_vocab()
